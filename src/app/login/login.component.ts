import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginFormGroup: FormGroup;
  constructor(private toastrService: ToastrService, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.initializeFormGroup();
  }

  initializeFormGroup = () => {
    this.loginFormGroup = new FormGroup({
      userName: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });
  }

  authenticate = () => {
    this.loginFormGroup.value;

    if (this.loginFormGroup.get('password').invalid && this.loginFormGroup.get('userName').invalid) {
      this.toastrService.error("User name and password is required.", "Error");
    } else {
      if (this.loginFormGroup.get('userName').invalid) {
        this.toastrService.error("User name is required.", "Error");
      }
      if (this.loginFormGroup.get('password').invalid) {
        this.toastrService.error("Password is required.", "Error");
      }
    }

    if (this.loginFormGroup.valid) {
      const authenticated: boolean = this.authService.authenticate(this.loginFormGroup.value);
      if (authenticated) {
        this.router.navigate(['dashboard']);
      } else {
        this.toastrService.error("Please check your user name password", "Error");
      }
    }
  }

}
