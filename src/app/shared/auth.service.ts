import { Injectable } from '@angular/core';
import * as users from '../data/users.json';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router) { }
  user: any;

  register = (user): boolean => {
    users.push(user);
    return true;
  }

  authenticate = (formValue: any): boolean => {
    this.user = users.find(x => x.userName === formValue.userName && x.password === formValue.password);
    if (this.user) {
      this.user.password = null;
      this.user.confirmPassword = null;
      this.setToStorage();
      return true;
    } else {
      return false;
    }
  }

  setToStorage = () => {
    if (this.user) {
      localStorage.setItem('user', JSON.stringify(this.user));
    }
  }

  getFromStorage = () => {
    return JSON.parse(localStorage.getItem('user'));
  }

  getUserName = () => {
    const user = this.getFromStorage();
    return user.firstName + ' ' + user.lastName;
  }

  getUserRole = () => {
    const user = this.getFromStorage();
    return user.role;
  }

  isLoggedIn = (): boolean => {
    return !!this.getFromStorage();
  }

  signOff = () => {
    localStorage.removeItem('user');
    this.router.navigate(['login']);

  }
}
