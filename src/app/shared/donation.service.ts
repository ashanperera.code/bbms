import { Injectable } from '@angular/core';
import * as donation from '../data/donation.json';
import { Donor } from './donor.model';

@Injectable({
    providedIn: 'root'
})
export class DonationService {
    public formData: any;

    donorList: Donor[];

    public status = [
        { status: "Consent Done" },
        { status: "Initiated" },
        { status: "Medical exam done" },
        { status: "Complete" },
        { status: "Incomplete" },
        { status: "Complete with reaction" },
        { status: "Incomplete with reaction" },
        { status: "Screening Done" },
        { status: "Label Printed" },
        { status: "Cancelled" }
    ]
    constructor() { }

    getCount = (status) => {
        return donation.filter(x => x.Status === status).length;
    }

    postDonation = (formData: any): boolean => {
        formData.donationId = donation.length + 1;
        donation.push(formData);
        return true;
    }

    getDonations = () => {
        return donation['default'];
    }

    putDonation = (formData: any): boolean => {
        const index = donation.findIndex(d => d.donationId == formData.donationId);
        donation['default'][index] = formData;
        return true;
    }

    deleteDonation = (id: number) => {
        donation.splice(donation.findIndex(x => x.donationId == id), 1);
        return true;
    }

}
