export class Donor {
    DonorID: number;
    FirstName: string;
    SecondName: string;
    thirdName: string;
    LastName: string;
    Gender: string;
    Nationality: string;
    BloodGroup: string;
    Status: string;
    LastDonation: Date;
}
