import { Injectable } from '@angular/core';
import * as inventory from '../data/inventory.json';

@Injectable({
    providedIn: 'root'
})
export class InventoryService {

    public status = [
        { status: "Hold" },
        { status: "Available" },
        { status: "Assigned" },
        { status: "Quarantine" },
        { status: "Expired" },
        { status: "Discarded" }
    ];

    constructor() { }

    getCount = (status) => {
        return inventory.filter(x => x.status === status).length;
    }

    postInventory = (formData: any): boolean => {
        formData.unitId = inventory.length + 1;
        inventory.push(formData);
        return true;
    }

    getInventory = () => {
        return inventory['default'];
    }

    putInventory = (formData: any): boolean => {
        const index = inventory.findIndex(d => d.unitId == formData.unitId);
        inventory['default'][index] = formData;
        return true;
    }

    deleteInventory = (id: number) => {
        inventory.splice(inventory.findIndex(x => x.unitId == id), 1);
        return true;
    }

}
