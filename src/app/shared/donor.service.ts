import { Injectable } from '@angular/core';
import { Donor } from './donor.model';
import * as dornerJson from '../data/dornor.json';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DonorService {

  formData: Donor;
  donorList: Donor[];

  public status = [
    { status: "Hold" },
    { status: "Accepted" },
    { status: "Deffered Tempory" },
    { status: "Deffered Permanently" },
    { status: "Blocked" }
  ]
  constructor() { }

  getCount = (status) => {
    return dornerJson.filter(x => x.Status === status).length;
  }

  postDonor = (formData: Donor): boolean => {
    formData.DonorID = dornerJson.length + 1;
    dornerJson.push(formData);
    return true;
  }

  getDonors = () => {
    return dornerJson['default'];
  }

  putDonor = (formData: Donor): boolean => {
    const index = dornerJson.findIndex(d => d.DonorID == formData.DonorID);
    dornerJson['default'][index] = formData;
    return true;
  }

  deleteDonor = (id: number) => {
    dornerJson.splice(dornerJson.findIndex(x => x.DonorID == id), 1);
    return true;
  }
}
