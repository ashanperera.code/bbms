import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { DonorService } from '../shared/donor.service';
import { DonationService } from '../shared/donation.service';
import { InventoryService } from '../shared/inventory.service';
import * as _ from 'underscore';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private donorService: DonorService, private donationService: DonationService, private inventoryService: InventoryService) { }

  ngOnInit() {
    this.initiatePiechart();
    this.initiateBarChart();
    this.initiateLineChart();
    this.multipleChart();
    this.donationBloodTotal();
  }

  initiatePiechart = () => {
    const status = this.donorService.status;
    const patientList: any[] = this.donorService.getDonors();
    status.map(x => {
      x['count'] = patientList.filter(p => p.Status === x.status).length;
      return x;
    });
    new Chart('pie-chart', {
      type: 'doughnut',
      data: {
        labels: status.map(x => x.status),
        datasets: [
          {
            data: status.map(x => x['count']),
            backgroundColor: ['#ffd246', '#4cb636', '#ff6358', '#ed2939', '#536878']
          }
        ]
      },
      options: {
        title: {
          display: false,
          text: 'Donors Status'
        },
        legend: {
          position: 'bottom',
          display: true,
          fullWidth: true,
          labels: {
            fontSize: 14
          }
        },
        scales: {
          xAxes: [{
            display: false
          }],
          yAxes: [{
            display: false
          }]
        }
      }
    });
  }

  initiateBarChart = () => {
    const status = this.donationService.status;
    const donations: any[] = this.donationService.getDonations();
    status.map(x => {
      x['count'] = donations.filter(p => p.Status === x.status).length;
      return x;
    });

    new Chart('bar-chart', {
      type: 'bar',
      data: {
        labels: status.map(x => x.status),
        datasets: [
          {
            data: status.map(x => x['count']),
            backgroundColor: 'rgba(99, 255, 132, 0.2)',
            borderColor: 'rgba(99, 255, 132, 1)',
            borderWidth: 1
          }
        ]
      },
      options: {
        title: {
          text: "BAR CHART",
          display: false
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    })
  }

  initiateLineChart = () => {
    const status = this.inventoryService.status;
    const donations: any[] = this.donationService.getDonations();
    const inventory: any[] = this.inventoryService.getInventory();

    let statusMapped: any[] = [];
    status.forEach(x => {
      let lineChartObject: any = {
        x: donations.filter(d => d.Dornor).length,
        y: inventory.filter(p => p.status === x.status).length
      }
      statusMapped.push(lineChartObject);
    });


    new Chart('line-chart', {
      type: 'line',
      data: {
        datasets: [{
          label: 'Höhenlinie',
          backgroundColor: "rgba(255, 99, 132,0.4)",
          borderColor: "rgb(255, 99, 132)",
          fill: true,
          data: statusMapped,
        }]
      },
      options: {
        responsive: true,
        title: {
          display: false,
          text: 'Höhenlinie'
        },
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              labelString: 'Länge',
              display: true,
            }
          }],
          yAxes: [{
            type: 'linear',
            scaleLabel: {
              labelString: 'Höhe',
              display: true
            }
          }]
        }
      }
    });
  }

  multipleChart = () => {
    const inventoryStatus: any[] = this.inventoryService.status;
    const inventory: any[] = this.inventoryService.getInventory();

    inventoryStatus.map(x => {
      x['count'] = inventory.filter(p => p.status === x.status).length;
      return x;
    });

    new Chart('inventory-chart', {
      type: 'bar',
      data: {
        labels: inventoryStatus.map(x => x.status),
        datasets: [
          {
            label: "counts",
            data: inventoryStatus.map(x => x['count']),
            backgroundColor: 'rgba(99, 255, 132, 0.2)',
            borderColor: 'rgba(99, 255, 132, 1)',
            borderWidth: 1
          }
        ]
      },
      options: {
        title: {
          text: "BAR CHART",
          display: false
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    })
  }

  donationBloodTotal = () => {
    const donors = this.donorService.getDonors();
    const groupedBloodTypes: any[] = _.groupBy(donors, 'BloodGroup');
    const keys = Object.keys(groupedBloodTypes);
    let dornorOutput: any[] = []
    keys.forEach(k => {
      let obj = {};
      obj['bloodGroup'] = k;
      obj['count'] = groupedBloodTypes[k].map(x => x.BloodGroup).length;
      dornorOutput.push(obj);
    })

    new Chart('pie-chart2', {
      type: 'doughnut',
      data: {
        labels: dornorOutput.map(x => x['bloodGroup']),
        datasets: [
          {
            data: dornorOutput.map(x => x['count']),
            backgroundColor: ['#ffd246', '#4cb636', '#ff6358', '#ed2939', '#536878']
          }
        ]
      },
      options: {
        title: {
          display: false,
          text: 'Groups'
        },
        legend: {
          position: 'bottom',
          display: true,
          fullWidth: true,
          labels: {
            fontSize: 14
          }
        },
        scales: {
          xAxes: [{
            display: false
          }],
          yAxes: [{
            display: false
          }]
        }
      }
    });
  }

  onDateChange(newDate: Date) {
    console.log(newDate);
  }
}

