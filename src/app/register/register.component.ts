import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../shared/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerFormGroup: FormGroup;
  constructor(private toastrService: ToastrService, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.initializeFormGroup();
  }

  initializeFormGroup = () => {
    this.registerFormGroup = new FormGroup({
      userName: new FormControl(null, Validators.required),
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      confirmPassword: new FormControl(null, Validators.required),
      role: new FormControl(null, Validators.required)
    });
  }

  register = () => {
    const password = this.registerFormGroup.get("password").value;
    const confirmPassword = this.registerFormGroup.get("confirmPassword").value;
    if (password === confirmPassword) {
      const registered = this.authService.register(this.registerFormGroup.value);
      this.router.navigate(['login']);
      if (registered) {
        this.toastrService.success("Regsiterd successfully.", "Success");
      }
    }
  }

}
