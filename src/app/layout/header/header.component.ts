import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LayoutComponent } from '../layout.component';
import { AuthService } from '../../shared/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  showDropDown: boolean = false;

  @Input() PARENT: LayoutComponent
  @Output() buttonClicked: EventEmitter<any> = new EventEmitter();
  userName: any;


  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.getUserName();
  }

  getUserName = () => {
    this.userName = this.authService.getUserName();
  }

  onProfileClick() {
    this.showDropDown = !this.showDropDown;
  }


  toggleSideMenu() {
    this.PARENT.toggleSideMenu();
  }

  onButtonClicked(name) {
    this.buttonClicked.emit(name);
  }

}
