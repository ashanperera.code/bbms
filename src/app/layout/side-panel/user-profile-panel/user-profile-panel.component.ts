import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../shared/auth.service';
@Component({
  selector: 'app-user-profile-panel',
  templateUrl: './user-profile-panel.component.html',
  styleUrls: ['./user-profile-panel.component.scss']
})
export class UserProfilePanelComponent implements OnInit {
  userName: string;
  userRole: any;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.getUserName();
    this.getUserRole();
  }

  getUserName = () => {
    this.userName = this.authService.getUserName();
  }

  getUserRole = () => {
    this.userRole = this.authService.getUserRole();
  }

  signOff = () => {
    this.authService.signOff();
  }
}
