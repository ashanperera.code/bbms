import { Component, OnInit, TemplateRef, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { DonationService } from '../shared/donation.service';
import { DonorService } from '../shared/donor.service';
import { InventoryService } from '../shared/inventory.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {

  modalRef: BsModalRef;
  formData: any = {};
  donationList: any[] = [];
  dornorList: any[] = [];
  selectedDornor: any = {};
  inventoryList: any[];

  constructor(
    private modalService: BsModalService,
    private toastr: ToastrService,
    private donationService: DonationService,
    private donorService: DonorService,
    @Inject('moment') private moment,
    private inventoryService: InventoryService) {
  }
  ngOnInit() {
    this.loadDonations();
    this.loadDornors();
    this.refreshDonations();
  }

  loadDonations = () => {
    this.donationList = this.donationService.getDonations();
  }

  loadDornors = () => {
    this.dornorList = this.donorService.getDonors();
  }

  inventoryStatuses = [
    {
      title: 'Hold',
      value: 0
    },
    {
      title: 'Available',
      value: 0
    },
    {
      title: 'Assigned',
      value: 0
    },
    {
      title: 'Quarantine',
      value: 0
    },
    {
      title: 'Expired',
      value: 0
    },
    {
      title: 'Discarded',
      value: 0
    }
  ]

  submit = (ngForm: NgForm) => {
    const formValues = ngForm.value;
    formValues['expireOn'] = this.formData.expireOn;
    if (formValues.unitId) {
      this.inventoryService.putInventory(formValues);
      this.toastr.info('Updated Successfully', 'Donor Register');
    } else {
      this.inventoryService.postInventory(formValues);
      this.toastr.success('Inserted Successfully', 'Donor Register');
    }
    this.refreshDonations();
    this.resetForm();
  }

  refreshDonations = () => {
    this.inventoryList = this.inventoryService.getInventory();
    this.updateStatus();
  }

  delete = (donationId) => {
    if (donationId) {
      this.inventoryService.deleteInventory(donationId);
    }
  }

  edit = (record: any, template: TemplateRef<any>) => {
    this.formData = record;
    this.donationOnchange(this.formData.Donation);
    this.openModal(template, record);
  }

  resetForm(form?: NgForm) {
    if (form) {
      form.resetForm();
    }
    this.formData = {
      DonorID: null,
      Gender: '',
      Nationality: '',
      BloodGroup: '',
      Status: '',
      LastDonation: null,
    };
  }

  updateStatus = () => {
    for (let index = 0; index < this.inventoryService.status.length; index++) {
      const count = this.inventoryService.getCount(this.inventoryService.status[index].status);
      const statTitle: string = this.inventoryService.status[index].status;
      this.inventoryStatuses[this.inventoryStatuses.findIndex(x => x.title === statTitle)].value = count;
    }
  }

  donationOnchange = (donationId) => {
    const dornorId = this.donationList.find(x => x.donationId === +donationId).Dornor;
    this.selectedDornor = this.dornorList.find(d => d.DonorID === dornorId);
    this.formData.dornor = this.selectedDornor.FirstName + ' ' + this.selectedDornor.LastName;
  }

  openModal(template: TemplateRef<any>, initialState?: any) {
    this.modalRef = this.modalService.show(template, {
      initialState: initialState,
      class: 'modal-xl'
    });
  }

  monthAutoMapper = (value) => {
    let createdDate = new Date(value);
    let month = createdDate.getMonth();
    createdDate.setMonth(month + 4);
    const datepipe: DatePipe = new DatePipe('en-US')
    this.formData.expireOn = this.moment(createdDate).format('D MMM YYYY');
  }
  onFilter = (value) => {
    if (value) {
      this.inventoryList = this.inventoryList.filter(v => v.status === value || v.unitId === +value);
    } else { 
      this.inventoryList = this.inventoryService.getInventory();
    }
  }
}
